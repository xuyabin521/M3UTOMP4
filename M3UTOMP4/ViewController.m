//
//  ViewController.m
//  test
//
//  Created by Develop on 2018/10/22.
//  Copyright © 2018 Develop. All rights reserved.
//

#import "ViewController.h"

@interface ViewController()

@property (strong , nonatomic) NSFileManager* fileMgr;

@end
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    NSMutableArray* arr = [self getFiles];
    NSLog(@"%@",arr);
    [self creatFileWithPath:@"/Users/develop/Downloads/newText.mp4"];
    
    [arr removeObject:@"/Users/develop/Downloads/aaaa/.DS_Store"];
    
    for (NSString* string in arr) {
        if ([string isEqualToString:@"/Users/develop/Downloads/aaaa/.DS_Store"] == NO) {
            NSLog(@"path:%@",string);
            NSFileHandle *fielHandle = [NSFileHandle fileHandleForUpdatingAtPath:@"/Users/develop/Downloads/newText.mp4"];
            [fielHandle seekToEndOfFile];
            [fielHandle writeData:[_fileMgr contentsAtPath:string]];
            [fielHandle closeFile];
        }
    }
}

//创建文件
- (BOOL)creatFileWithPath:(NSString *)filePath
{
    BOOL isSuccess = YES;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL temp = [fileManager fileExistsAtPath:filePath];
    if (temp) {
        return YES;
    }
    NSError *error;
    //stringByDeletingLastPathComponent:删除最后一个路径节点
    NSString *dirPath = [filePath stringByDeletingLastPathComponent];
    isSuccess = [fileManager createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:&error];
    if (error) {
        NSLog(@"creat File Failed. errorInfo:%@",error);
    }
    if (!isSuccess) {
        return isSuccess;
    }
    isSuccess = [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    return isSuccess;
}



- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

-(NSMutableArray*)getFiles{
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:10];
    
    _fileMgr = [NSFileManager defaultManager];
    
    NSArray* tempArray = [_fileMgr contentsOfDirectoryAtPath:@"/Users/develop/Downloads/aaaa/" error:nil];
    
    NSMutableArray* arr = [NSMutableArray arrayWithArray:tempArray];
    [arr removeObject:@".DS_Store"];
    
    NSArray* newArr =  [arr sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSInteger str1 = [[[(NSString*) obj1 componentsSeparatedByString:@"_"] objectAtIndex:1] integerValue];
        NSInteger str2 = [[[(NSString*) obj2 componentsSeparatedByString:@"_"] objectAtIndex:1] integerValue];
        if (str1>str2) {
            return YES;
        } else {
            return NO;
        }
    }];
    
    for (NSString* fileName in newArr) {
        BOOL flag = YES;
        NSString* fullPath = [@"/Users/develop/Downloads/aaaa/" stringByAppendingPathComponent:fileName];
        
        if ([_fileMgr fileExistsAtPath:fullPath isDirectory:&flag]) {
            if (!flag) {
                [array addObject:fullPath];
            }
        }
    }
    return array;
}

@end
